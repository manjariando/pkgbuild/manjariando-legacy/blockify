# Maintainer: demian <mikar ατ gmx δοτ de>

pkgname=blockify
pkgver=3.6.3
pkgrel=3.2
pkgdesc="Mutes Spotify advertisements."
arch=("any")
url="https://github.com/mikar/${pkgname}"
license=("MIT")
conflicts=("blockify-git")
makedepends=("python-setuptools")
optdepends=("pulseaudio: mute spotify sink instead of system sound")
source=("blockify-${pkgver}.tar.gz::${url}/archive/v${pkgver}.tar.gz"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml")
sha256sums=('7d4bb3a4b759386141ba00513b1d5c7ab98bf0300b8c5558f2d8a2001fccdfe2'
            '5eb18455f4ace6ca258b751661c13cfa062b3049dfdb7a7a563e282636534845')

_blockify_desktop="[Desktop Entry]
Name=Blockify
Comment=Blocks Spotify commercials
Exec=blockify-ui
Icon=blockify.png
Type=Application
StartupNotify=true
Categories=AudioVideo;"

build() {
    cd "${srcdir}"
    echo -e "$_blockify_desktop" | tee com.${pkgname}.desktop
}

package() {
    depends=("spotify"
            "alsa-utils"
            "pygtk"
            "gst-python"
            "python-dbus"
            "python-docopt"
            "wmctrl")

    cd "${srcdir}"/${pkgname}-${pkgver}

    python3 setup.py install --root="${pkgdir}"

    install -Dm644 "${pkgname}/data/icon-blue-512.png" "${pkgdir}/usr/share/pixmaps/${pkgname}.png"
    install -Dm644 "${srcdir}/com.${pkgname}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "$pkgdir/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for i in 512; do
        install -Dm644 "${pkgname}/data/icon-blue-${i}.png" \
            "${pkgdir}/usr/share/icons/hicolor/${i}x${i}/apps/${pkgname}.png"
    done
}
